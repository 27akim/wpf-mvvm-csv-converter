﻿using System;

namespace CStarterTaskWpf
{
    class DataIncorrectExeption : ArgumentException
    {
        public string Word { get; } //Variable to store the passed string
        public int Row { get; }
        public DataIncorrectExeption(int row, string message, string csvString="") : base(message) //Gets string and error message
        {
            Word = csvString;
            Row = row;
        }
    }
}
