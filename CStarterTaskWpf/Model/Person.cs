﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CStarterTaskWpf
{
    public class Person : INotifyPropertyChanged
    {
        private int _id;
        private DateTime date;
        private string _name;
        private string _surname;
        private string _middlename;
        private string _city;
        private string _country;

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }
        public DateTime Date
        {
            get { return date; }
            set
            {
                date = value;
                OnPropertyChanged(nameof(Date));
            }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        public string Surname
        {
            get { return _surname; }
            set
            {
                _surname = value;
                OnPropertyChanged(nameof(Surname));
            }
        }
        public string Middlename
        {
            get { return _middlename; }
            set
            {
                _middlename = value;
                OnPropertyChanged(nameof(Middlename));
            }
        }
        public string City
        {
            get { return _city; }
            set
            {
                _city = value;
                OnPropertyChanged(nameof(City));
            }
        }
        public string Country
        {
            get { return _country; }
            set
            {
                _country = value;
                OnPropertyChanged(nameof(Country));
            }
        }

        public Person()
        {

        }

        public Person(DateTime dateValue, string nameValue, string surnameValue, string middlenameValue,
            string cityValue, string countryValue)
        {
            date = dateValue;
            _name = nameValue;
            _surname = surnameValue;
            _middlename = middlenameValue;
            _city = cityValue;
            _country = countryValue;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
