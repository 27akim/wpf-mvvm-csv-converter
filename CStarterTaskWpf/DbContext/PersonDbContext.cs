﻿using System.Data.Entity;

namespace CStarterTaskWpf
{
    public class PersonDbContext : DbContext
    {
        public PersonDbContext() : base("DataConnection")
        {
            Database.SetInitializer<PersonDbContext>(new DropCreateDatabaseAlways<PersonDbContext>());
        }

        public DbSet<Person> Persons { get; set; }


    }
}
