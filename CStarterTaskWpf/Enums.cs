﻿namespace CStarterTaskWpf
{
    enum PersonData
    {
        Date,
        Name,
        Surname,
        Middlename,
        City,
        Country
    }
}
