﻿using System.Windows;

namespace CStarterTaskWpf
{
    /// <summary>
    /// Логика взаимодействия для ConvertWindow.xaml
    /// </summary>
    public partial class ConvertWindow : Window
    {
        private string format;
        public ConvertWindow(string _format)
        {
            format = _format;
            InitializeComponent();
        }

        private void Export_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Done!\n" + System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)
                + @"\data." + format);
            this.DialogResult = true;
        }
    }
}
