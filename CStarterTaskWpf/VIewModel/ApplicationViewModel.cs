﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using MVVM;
using System.IO;
using System.Data;
using System.Xml.Linq;
using System.Threading.Tasks;
using CStarterTaskWpf.View;
using System.Data.Entity;
using System.Diagnostics;

namespace CStarterTaskWpf
{
    class ApplicationViewModel : INotifyPropertyChanged
    {
        private const int DataElementsLength = 6;
        //max page size of dataGridView
        private int _pageSize = 15; 
        private int _pageNumber = 0;
        //variable for .csv file
        private FileStream file;
        private PersonDbContext context;
        private IOpenFileService openFileService;
        private List<Person> personList;
        //variable for sorting the list
        private bool filterAscending = true;
        private string filterProperty = "Id";
        //collection that connected with listView
        public ObservableCollection<Person> Persons { get; set; }

        public ApplicationViewModel(IOpenFileService openFileService)
        {
            Persons = new ObservableCollection<Person>();
            this.openFileService = openFileService;
            //hide loader
            ProgressVisibility = System.Windows.Visibility.Hidden;
            //hide errors page
            ErrorPageVisibility = System.Windows.Visibility.Hidden;
        }

        private void ReadDataLine()
        {
            //show loader
            ProgressVisibility = System.Windows.Visibility.Visible;
            ErrorPageVisibility = System.Windows.Visibility.Hidden;
            OpenButtonEnabled = false;
            ExportButtonsEnabled = false;
            //enable page buttons if collection is not empty
            if (personList.Count() != 0)
            {
                PageButtonsEnabled = true;
            }
            StringBuilder errors = new StringBuilder();
            string str;
            using (StreamReader sr = new StreamReader(file))
            {
                //reading file line by line
                for (int i = 0; (str = sr.ReadLine()) != null; i++) 
                {
                    string[] data = str.Split(';');
                    try
                    {
                        //creating new Person
                        CreatePerson(data, i); 
                    }
                    //if file contains some wrong data, throw error
                    catch (DataIncorrectExeption ex) 
                    {
                        errors.Append("Incorrect data in row №" + ex.Row + "\n" + ex.Message + ex.Word + "\n\n");
                    }
                }
            }
            //hide loader
            ProgressVisibility = System.Windows.Visibility.Hidden;
            OpenButtonEnabled = true;
            if (personList.Count() != 0)
            {
                ExportButtonsEnabled = true;
                PageButtonsEnabled = true;
            }
            //check if data has errors
            ErrorsList = "";
            if (errors.Length != 0)
            {
                openFileService.ShowMessage("Your data contains errors!\nCheck the errors page!");
                ErrorsList = errors.ToString();
                ErrorPageVisibility = System.Windows.Visibility.Visible;
            } 
            App.Current.Dispatcher.Invoke((System.Action)delegate
            {
                //update collection that connected with listView
                UpdateListView(context.Persons.OrderBy(j => j.Id).Take(_pageSize).ToList());
            });
        }

        private void CreatePerson(string[] data, int row)
        {
            //value for viewing row that contains error
            row++;
            //cheking if data contains errors
            CheckData(data, row); 
            //creating Person
            Person person = new Person(DateTime.Parse(data[(int)PersonData.Date]), data[(int)PersonData.Name], data[(int)PersonData.Surname],
                data[(int)PersonData.Middlename], data[(int)PersonData.City], data[(int)PersonData.Country]);
            AddToDataBase(person); 
        }

        private void AddToDataBase(Person personValue)
        {
            using (PersonDbContext db = new PersonDbContext())
            {
                db.Persons.Add(personValue);
                personList.Add(personValue);
                db.SaveChanges();
            }
        }

        private void CheckData(string[] data, int row)
        {
            foreach(string str in data)
            {
                if (str.Length < 1)
                {
                    throw new DataIncorrectExeption(row, "Incomplete data!");
                }
            }
            CheckDate(data[(int)PersonData.Date], row);
        }

        private void CheckDate(string date, int row)
        {
            DateTime dateValue;
            if (!(DateTime.TryParse(date, out dateValue)))
            {
                throw new DataIncorrectExeption(row, "Incorrect date: ", date);
            }
        }

        //shows export window and returns list
        public List<Person> ShowExportWindow(string format) 
        {
            ConvertWindow convertWindow = new ConvertWindow(format);
            if (convertWindow.ShowDialog() == true)
            {
                //if data filter is empty, define some data
                if(convertWindow.textBoxDate.Text.Length == 0)
                {
                    convertWindow.textBoxDate.Text = "01.01.1900";
                }
                DateTime dtFrom = DateTime.ParseExact(convertWindow.textBoxDate.Text, "dd.MM.yyyy", null);
                return context.Persons.Where(p => (!convertWindow.checkBoxDate.IsChecked.Value || p.Date == dtFrom)
  && (!convertWindow.checkBoxName.IsChecked.Value || p.Name == convertWindow.textBoxName.Text) && (!convertWindow.checkBoxSurname.IsChecked.Value || p.Surname == convertWindow.textBoxSurname.Text)
  && (!convertWindow.checkBoxMiddlename.IsChecked.Value || p.Middlename == convertWindow.textBoxMiddlename.Text) && (!convertWindow.checkBoxCity.IsChecked.Value || p.City == convertWindow.textBoxCity.Text)
  && (!convertWindow.checkBoxCountry.IsChecked.Value || p.Country == convertWindow.textBoxCountry.Text)).ToList();
            }
            return null;
        }

        private void ExportXml(List<Person> list)
        {
            XDocument xdoc = new XDocument();
            XElement mainElem = new XElement("TestProgramm");
            foreach (Person p in list)
            {
                XElement recordElem = new XElement("Record");
                XAttribute idAttr = new XAttribute("id", p.Id);
                XElement dateElem = new XElement("Date", p.Date.ToString("dd.MM.yyyy"));
                XElement nameElem = new XElement("Name", p.Name);
                XElement surnameElem = new XElement("Surname", p.Surname);
                XElement middlenameElem = new XElement("Middlename", p.Middlename);
                XElement cityElem = new XElement("City", p.City);
                XElement countryElem = new XElement("Country", p.Country);
                recordElem.Add(idAttr);
                recordElem.Add(dateElem);
                recordElem.Add(nameElem);
                recordElem.Add(surnameElem);
                recordElem.Add(middlenameElem);
                recordElem.Add(cityElem);
                recordElem.Add(countryElem);
                mainElem.Add(recordElem);
            }
            xdoc.Add(mainElem);
            xdoc.Save("data.xml");
        }

        private void ExportExcel(List<Person> list)
        {
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApp.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel.Worksheet excelWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)excelWorkbook.ActiveSheet;
            excelWorksheet.Name = "Persons";
            //making top of the table
            excelWorksheet.Cells[1, 1] = "Id";
            excelWorksheet.Cells[1, 2] = "Date";
            excelWorksheet.Cells[1, 3] = "Name";
            excelWorksheet.Cells[1, 4] = "Surname";
            excelWorksheet.Cells[1, 5] = "Middlename";
            excelWorksheet.Cells[1, 6] = "City";
            excelWorksheet.Cells[1, 7] = "Country";
            //filling from the second row
            int i = 2; 
            foreach (Person p in list)
            {
                //column number
                int j = 1; 
                excelWorksheet.Cells[i, j] = p.Id;
                excelWorksheet.Cells[i, ++j] = p.Date.ToString("dd.MM.yyyy");
                excelWorksheet.Cells[i, ++j] = p.Name;
                excelWorksheet.Cells[i, ++j] = p.Surname;
                excelWorksheet.Cells[i, ++j] = p.Middlename;
                excelWorksheet.Cells[i, ++j] = p.City;
                excelWorksheet.Cells[i, ++j] = p.Country;
                i++;
            }
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            excelWorkbook.SaveAs(path + "/data.xls");
            excelWorkbook.Close(true, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
            excelApp.Quit();
        }

        private void UpdateListView(List<Person> list)
        {
            Persons.Clear();
            foreach (Person person in list)
            {
                Persons.Add(person);
            }
        }

        private void CheckFilterUpdateList()
        {
            if (!filterAscending)
                UpdateListView(personList.OrderByDescending(u => u.GetType().GetProperty(filterProperty.ToString()).GetValue(u)).Skip(_pageSize * _pageNumber).Take(_pageSize).ToList());
            else
                UpdateListView(personList.OrderBy(u => u.GetType().GetProperty(filterProperty.ToString()).GetValue(u)).Skip(_pageSize * _pageNumber).Take(_pageSize).ToList());
        }

        //**************************************************Commands***********************************************************

        //opening .csv file
        private RelayCommand openCommand;
        public RelayCommand OpenCommand
        {
            get
            {
                return openCommand ??
                  (openCommand = new RelayCommand(obj =>
                  {
                      if (openFileService.OpenFileDialog() == true)
                      {
                          file = new FileStream(openFileService.FilePath, FileMode.Open,
                              FileAccess.Read, FileShare.ReadWrite);
                          //start reading data in new thread
                          new Task(() =>
                          {
                              ReadDataLine();
                          }).Start();
                      }
                  }));
            }
        }

        private RelayCommand nextPageCommand;
        public RelayCommand NextPageCommand
        {
            get
            {
                return nextPageCommand ??
                  (nextPageCommand = new RelayCommand(obj =>
                  {
                      //cheking if next page exist
                      if ((context.Persons.Count() / _pageSize) <= _pageNumber) 
                      {
                          return;
                      }
                      _pageNumber++;
                      CheckFilterUpdateList();
                  }));
            }
        }

        private RelayCommand previosPageCommand;
        public RelayCommand PreviosPageCommand
        {
            get
            {
                return previosPageCommand ??
                  (previosPageCommand = new RelayCommand(obj =>
                  {
                      //cheking if next page exist
                      if (_pageNumber == 0) 
                      {
                          return;
                      }
                      _pageNumber--;
                      CheckFilterUpdateList();
                  }));
            }
        }

        private RelayCommand exportExcelCommand;
        public RelayCommand ExportExcelCommand
        {
            get
            {
                return exportExcelCommand ??
                  (exportExcelCommand = new RelayCommand((exportFormat) =>
                  {
                      List<Person> list = ShowExportWindow("xls");
                      if (list != null)
                      {
                          ExportExcel(list);
                      }
                  }));
            }
        }

        private RelayCommand exportXmlCommand;
        public RelayCommand ExportXmlCommand
        {
            get
            {
                return exportXmlCommand ??
                  (exportXmlCommand = new RelayCommand((exportFormat) =>
                  {
                      List<Person> list = ShowExportWindow("xml");
                      if (list != null)
                      {
                          ExportXml(list);
                      }
                  }));
            }
        }

        private RelayCommand loadedCommand;
        public RelayCommand LoadedCommand
        {
            get
            {
                return loadedCommand ??
                  (loadedCommand = new RelayCommand((obj) =>
                  {
                      context = new PersonDbContext();
                      context.SaveChanges();
                      //Database.SetInitializer<PersonDbContext>(null);
                      personList = context.Persons?.ToList();
                      OpenButtonEnabled = true;
                      //update collection that connected with listView
                      UpdateListView(personList.OrderBy(j => j.Id).Take(_pageSize).ToList());
                      if (personList.Count > 0)
                      {
                          //enable other buttons if collection is not empty
                          ExportButtonsEnabled = true;
                          PageButtonsEnabled = true;
                      }
                  }));
            }
        }

        private RelayCommand removeItem;
        public RelayCommand RemoveItem
        {
            get
            {
                return removeItem ??
                  (removeItem = new RelayCommand((obj) =>
                  {
                      Person person = obj as Person;
                      if (person != null)
                      {
                          //p.Remove(person);
                          personList.Remove(person);
                          context.Persons.Remove(person);
                          context.SaveChanges();
                          CheckFilterUpdateList();
                      }
                  },
            (obj) => Persons.Count > 0));
            }
        }

        private RelayCommand editItem;
        public RelayCommand EditItem
        {
            get
            {
                return editItem ??
                  (editItem = new RelayCommand((obj) =>
                  {
                      Person person = obj as Person;
                      if (person != null)
                      {
                          EditWindow editWindow = new EditWindow();
                          editWindow.textBoxDate.Text = person.Date.ToString("dd.MM.yyyy");
                          editWindow.textBoxName.Text = person.Name;
                          editWindow.textBoxSurname.Text = person.Surname;
                          editWindow.textBoxMiddlename.Text = person.Middlename;
                          editWindow.textBoxCity.Text = person.City;
                          editWindow.textBoxCountry.Text = person.Country;
                          if (editWindow.ShowDialog() == true)
                          {
                              var result = context.Persons.SingleOrDefault(b => b.Id == person.Id);
                              if (result != null)
                              {
                                  string[] str = { editWindow.textBoxDate.Text, editWindow.textBoxName.Text,
                                      editWindow.textBoxSurname.Text, editWindow.textBoxMiddlename.Text, editWindow.textBoxCity.Text,
                                      editWindow.textBoxCity.Text};
                                  try
                                  {
                                      CheckData(str, 0);
                                      result.Date = DateTime.ParseExact(editWindow.textBoxDate.Text, "dd.MM.yyyy", null);
                                      result.Name = editWindow.textBoxName.Text;
                                      result.Surname = editWindow.textBoxSurname.Text;
                                      result.Middlename = editWindow.textBoxMiddlename.Text;
                                      result.City = editWindow.textBoxCity.Text;
                                      result.Country = editWindow.textBoxCountry.Text;
                                      context.SaveChanges();
                                      UpdateListView(personList.OrderByDescending(u => u.GetType().GetProperty(filterProperty.ToString()).GetValue(u)).Skip(_pageSize * _pageNumber).Take(_pageSize).ToList());
                                  }
                                  catch(DataIncorrectExeption ex)
                                  {
                                      openFileService.ShowMessage("You have written incorrect data!" + "\n" + ex.Message + ex.Word + "\n\n");
                                  }
                              }
                          }
                      }
                  },
            (obj) => Persons.Count > 0));
            }
        }

        private RelayCommand sortCommand;
        public RelayCommand SortCommand
        {
            get
            {
                return sortCommand ??
                  (sortCommand = new RelayCommand((obj) =>
                  {
                      filterProperty = obj as string;
                      filterAscending = !filterAscending;
                      if (!filterAscending)
                      {
                          personList = personList.OrderByDescending(u => u.GetType().GetProperty(filterProperty.ToString()).GetValue(u)).ToList();
                          UpdateListView(personList.OrderByDescending(u => u.GetType().GetProperty(filterProperty.ToString()).GetValue(u)).Skip(_pageSize * _pageNumber).Take(_pageSize).ToList());
                      } 
                      else
                      {
                          personList = personList.OrderBy(u => u.GetType().GetProperty(filterProperty.ToString()).GetValue(u)).ToList();
                          UpdateListView(personList.OrderBy(u => u.GetType().GetProperty(filterProperty.ToString()).GetValue(u)).Skip(_pageSize * _pageNumber).Take(_pageSize).ToList());
                      }
                  }));
            }
        }


        //**************************************************Properties***********************************************************

        //enable/disable other buttons
        private bool pageButtonsEnabled;
        public bool PageButtonsEnabled
        {
            get => pageButtonsEnabled;
            set
            {
                pageButtonsEnabled = value;
                OnPropertyChanged(nameof(PageButtonsEnabled));
            }
        }

        private bool openButtonEnabled;
        public bool OpenButtonEnabled
        {
            get => openButtonEnabled;
            set
            {
                openButtonEnabled = value;
                OnPropertyChanged(nameof(OpenButtonEnabled));
            }
        }

        private bool exportButtonsEnabled;
        public bool ExportButtonsEnabled
        {
            get => exportButtonsEnabled;
            set
            {
                exportButtonsEnabled = value;
                OnPropertyChanged(nameof(ExportButtonsEnabled));
            }
        }

        private string errorsList;
        public string ErrorsList
        {
            get => errorsList;
            set
            {
                errorsList = value;
                OnPropertyChanged(nameof(ErrorsList));
            }
        }

        private System.Windows.Visibility progressVisibility;
        public System.Windows.Visibility ProgressVisibility
        {
            get => progressVisibility;
            set
            {
                progressVisibility = value;
                OnPropertyChanged(nameof(ProgressVisibility));
            }
        }

        private System.Windows.Visibility errorPageVisibility;
        public System.Windows.Visibility ErrorPageVisibility
        {
            get => errorPageVisibility;
            set
            {
                errorPageVisibility = value;
                OnPropertyChanged(nameof(ErrorPageVisibility));
            }
        }

        private Person selectedPerson;
        public Person SelectedPerson
        {
            get { return selectedPerson; }
            set
            {
                selectedPerson = value;
                OnPropertyChanged(nameof(SelectedPerson));
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
