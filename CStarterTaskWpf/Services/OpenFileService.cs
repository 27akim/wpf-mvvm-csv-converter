﻿using Microsoft.Win32;
using System.Windows;

namespace CStarterTaskWpf
{
    class OpenFileService : IOpenFileService
    {
        public string FilePath { get; set; }

        public void ShowMessage(string text)
        {
            MessageBox.Show(text);
        }

        public bool OpenFileDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "(*.csv) | *.csv";
            if (openFileDialog.ShowDialog() == true)
            {
                FilePath = openFileDialog.FileName;
                return true;
            }
            return false;
        }
    }
}
