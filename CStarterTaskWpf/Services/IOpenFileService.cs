﻿namespace CStarterTaskWpf
{
    public interface IOpenFileService
    {
        string FilePath { get; set; }
        void ShowMessage(string message);  
        bool OpenFileDialog();  
    }
}
